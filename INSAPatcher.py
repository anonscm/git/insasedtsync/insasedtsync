#!/usr/bin/python3
# -*- coding: utf-8 -*-

import distutils.dir_util
import logging
import os
import re
import shutil
import stat
import subprocess
import sys


logformat = '%(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=logformat)
logger = logging.getLogger()

class CommandError(Exception): pass

class PatchError(Exception): pass
class InvalidPathError(Exception): pass
class IsDirectoryError(Exception): pass
class DestinationExistsError(Exception): pass

class NoChangeError(Exception): pass


def askUserOnError(f):
	def applicator(*args, **kwargs):
		while True:
			try:
				ret = f(*args,**kwargs)
			except Exception as e:
				logger.error(e)
			else:
				return ret
			
			choice="UNDEF"
			while choice not in ("a", "i", "r"):
				choice = input("What do you want to do ? [R]etry, [I]gnore, [A]bort ")
				choice = choice.lower()
			if choice == "a":
				raise SystemExit(0)
			elif choice == "i":
				return None

	return applicator


class Command:
	DEVNULL = subprocess.DEVNULL
	TOVAR   = subprocess.PIPE
	SYSDEFAULT = None
	
	@staticmethod
	@askUserOnError
	def run(cmdlist, stdout=DEVNULL, stderr=DEVNULL, stdin=DEVNULL):
		""" Run specified external command
		@param cmdlist: list containing command to run and args if any
		@type  cmdlist: [str]
		@param stdout: if Command.TOVAR, return stdout in var, if Command.SYSDEFAULT keep it as default, and if Command.DEVNULL, ignore it
		@type  stdout: int
		@param stderr: if Command.TOVAR, return stdout in var, if Command.SYSDEFAULT keep it as default, and if Command.DEVNULL, ignore it
		@type  stderr: int
		@param stdin: if Command.SYSDEFAULT keep it as default, and if Command.DEVNULL, ignore it
		@type  stdin: int
		@return: (command exit code, stdout, stderr)
		@rtype: (int, str, str)
		"""
		outputencoding = sys.stdout.encoding
		
		try:
			(out, err) = ("", "")
			p = subprocess.Popen(cmdlist, stdout=stdout, stderr=stderr, stdin=stdin)
			retcode = None
			while retcode == None:
				(o, e) = p.communicate()
				if stdout == Command.TOVAR: out += o.decode(outputencoding)
				if stderr == Command.TOVAR: err += e.decode(outputencoding)
				retcode = p.poll()
		except OSError as e:
			(retcode, out, err) = (-9999999, "", str(e))
		
		if retcode != 0:
			raise CommandError("Command\n  %s\nfailed (retcode:%s)" % (" ".join(cmdlist), retcode))
		return (retcode, out, err)

	

class Android:
	_apksigner = None # Will be set on first call to signApk
	_buildnumber = None
	
	@staticmethod
	@askUserOnError
	def getNextBuildNumber(buildnumbercachefile):
		if not Android._buildnumber and os.path.isfile(buildnumbercachefile):
			with open(buildnumbercachefile, 'r', encoding="utf8") as f:
				Android._buildnumber = int(f.read()) + 1
		else:
			Android._buildnumber = 1
		
		return Android._buildnumber
	
	
	@staticmethod
	@askUserOnError
	def commitCurrentBuild(buildnumbercachefile):
		with open(buildnumbercachefile, 'w', encoding="utf8") as f:
			f.write(str(Android._buildnumber))
	
	
	@staticmethod
	@askUserOnError
	def _checkRequirements():
		if 'ANDROID_HOME' not in os.environ:
			raise AssertionError("Android : required env var ANDROID_HOME is not set")
		
		builddir = Filesystem.unixPathToOS(
			"%s/build-tools" % (os.environ['ANDROID_HOME'])
		)
		
		if not os.path.isdir(builddir):
			raise AssertionError("Android : required 'build-tools' dir under ANDROID_HOME not found")

		versions = []
		for d in os.listdir(builddir):
			path = builddir + os.sep + d
			if os.path.isdir(path):
				versions.append(path)

		if len(versions) == 0:
			raise AssertionError("Android : No version of android build-tools seems installed")

		for version in sorted(versions, reverse=True):
			Android._apksigner = version + os.sep + "apksigner.bat"
			if os.path.isfile(Android._apksigner):
				break
		else:
			raise AssertionError("Android : apksigner.bat not found")


	@staticmethod
	@askUserOnError
	def signApk(apksrc, apkdst, keystore, keyalias=None):
		if not Android._apksigner:
			Android._checkRequirements()
		
		src = Filesystem.unixPathToOS(apksrc)
		dst = Filesystem.unixPathToOS(apkdst)
		ks = Filesystem.unixPathToOS(keystore)
		
		if not os.path.isfile(src):
			raise InvalidPathError("signApk : apk source not found (%s)" % src)
		
		if not os.path.isfile(ks):
			raise InvalidPathError("signApk : keystore not found (%s)" % ks)
		
		if keyalias:
			cmd = [Android._apksigner, "sign", "--ks", ks, "--ks-key-alias", keyalias, "--out", dst, src]
		else:
			cmd = [Android._apksigner, "sign", "--ks", ks, "--out", dst, src]
		
		(ret, out, err) = Command.run(cmd, stdout=Command.SYSDEFAULT, stdin=Command.SYSDEFAULT)
		if ret != 0:
			# Ask retry, ignore abort
			pass


class SourceFile:
	""" Handle source file content checking and patching """
	
	def __init__(self, path):
		self._path = Filesystem.unixPathToOS(path)
		self._patchqueue = []
		with open(path, 'r', encoding="utf8") as f:
			self._content = f.read()
	
	
	def content(self):
		return self._content
	
	
	@askUserOnError
	def isSameAs(other):
		if type(other) == str:
			# Other is a path
			o = SourceFile(other)
		elif type(other) == SourceFile:
			o = other
		else:
			raise TypeError("Asked SourceFile.isSameAs with arg of type '%s', instead of str or SourceFile" % (type(other)))
		
		return self.content() == other.content()
	
	
	def hasContent(self, content):
		return content in self.content()
	
	
	def _replace(self, replaced, saveContent=True):
		if replaced != self.content():
			self._content = replaced
			if saveContent:
				self.saveContent()
			return True
		
		return False
	
	
	@askUserOnError
	def saveContent(self):
		with open(self._path, 'w', encoding="utf8", newline="\n") as f:
			f.write(self._content)
	
	
	@askUserOnError
	def processReplaceQueue(self):
		while(len(self._patchqueue) > 0):
			(fnReplace, arg1, arg2, failsWhenUnchanged) = self._patchqueue.pop()
			replaceOk = False
			while not replaceOk: 
				try:
					replaceOk = fnReplace(self, arg1, arg2, failsWhenUnchanged, False)
				except NoChangeError as e:
					logger.debug("processReplaceQueue caught NoChangeError(%s)" % e)
					raise(e)
				if not failsWhenUnchanged:
					replaceOk = True
		
		self.saveContent()
	
	
	def enqueueReplace(self, old, new, failsWhenUnchanged=True):
		self._patchqueue.insert(0, (SourceFile.replace, old, new, failsWhenUnchanged))
	
	
	@askUserOnError
	def replace(self, old, new, failsWhenUnchanged=True, saveContent=True):
		replaced = self._content.replace(old, new)
		res = self._replace(replaced, saveContent)
		if not res and failsWhenUnchanged:
			raise NoChangeError("Source file '%s', replace :\n '%s'\nby\n '%s'\nchanged nothing" % (self._path, old, new))
		return res
	
	
	def enqueueReplaceByRegex(self, pattern, replacement, failsWhenUnchanged=True):	
		self._patchqueue.insert(0, (SourceFile.replaceByRegex, pattern, replacement, failsWhenUnchanged))
	
	
	@askUserOnError
	def replaceByRegex(self, pattern, replacement, failsWhenUnchanged=True, saveContent=True):
		replaced = re.sub(pattern, replacement, self._content, flags=re.MULTILINE)
		res = self._replace(replaced, saveContent)
		if not res and failsWhenUnchanged:
			raise NoChangeError("Source file '%s', replace by regex :\n '%s'\nby\n '%s'\nchanged nothing" % (self._path, pattern, replacement))
		return res


class Filesystem:
	""" Simple files / directories operations """
	
	@staticmethod
	@askUserOnError
	def unixPathToOS(path):
		return os.path.normpath(path)
	
	@staticmethod
	@askUserOnError
	def mkdir(path):
		p = Filesystem.unixPathToOS(path)
		if not os.path.isdir(p):
			logger.debug("%s> mkdir(%s)" % (os.getcwd(), p))
			os.mkdir(p)
	
	@staticmethod
	@askUserOnError
	def findFiles(path, prefixes=(), suffixes=(), filesOnly=False, dirsOnly=False):
		found = []
		for root, dirs, files in os.walk(path):
			if filesOnly:
				toCheck = files
			elif dirsOnly:
				toCheck = dirs
			else:
				toCheck = dirs + files
				
			for item in toCheck:
				if (not prefixes or (prefixes and item.startswith(prefixes))) \
				and (not suffixes or (suffixes and item.endswith(suffixes))):
					 found.append(os.path.join(root, item))
		return found
	
	
	@staticmethod
	def rmtreeSetRW(action, path, exc):
		try:
			os.chmod(path, stat.S_IWRITE)
			os.remove(path)
		except Exception as e:
			raise IOError("Unable to remove '%s'" % (path))
	
	
	@staticmethod
	@askUserOnError
	def rmtree(path):
		p = Filesystem.unixPathToOS(path)
		if os.name == 'nt':
			# Enable long paths on Windows
			p = '\\\\?\\' + os.path.abspath(p)
			
		if os.path.lexists(p) and not os.path.isdir(p):
			raise InvalidPathError("rmtree called to remove something else than a directory (%s)" % (p))
			
		if not os.path.isdir(p):
			return
			
		while(os.path.isdir(p)):
			logger.debug("%s> rmtree(%s)" % (os.getcwd(), p))
			shutil.rmtree(p, ignore_errors=False, onerror=Filesystem.rmtreeSetRW)
	
	
	@staticmethod
	@askUserOnError
	def copytree(srcpath, dstpath):
		src = Filesystem.unixPathToOS(srcpath)
		dst = Filesystem.unixPathToOS(dstpath)
		
		if not os.path.isdir(src):
			raise InvalidPathError("copytree called to copy a non existent directory (%s)" % (src))
		
		if os.path.lexists(dst):
			raise DestinationExistsError("copytree called with an existant destination (%s)" % (dst))
		
		logger.debug("%s> copytree('%s' to '%s')" % (os.getcwd(), src, dst))
		shutil.copytree(src, dst)
	
	
	@staticmethod
	@askUserOnError
	def copydircontent(srcpath, dstpath):
		src = Filesystem.unixPathToOS(srcpath)
		dst = Filesystem.unixPathToOS(dstpath)
		
		if not os.path.isdir(src):
			raise InvalidPathError("copydircontent called to copy from a non existent directory (%s)" % (src))
		
		if os.path.lexists(dst) and not os.path.isdir(dst):
			raise InvalidPathError("copytree called with an existant destination that is not a directory (%s)" % (dst))
		
		logger.debug("%s> copydircontent('%s' to '%s')" % (os.getcwd(), src, dst))
		distutils.dir_util.copy_tree(src, dst)
	
	
	@staticmethod
	@askUserOnError
	def copyfile(srcpath, dstpath):
		src = Filesystem.unixPathToOS(srcpath)
		dst = Filesystem.unixPathToOS(dstpath)
		
		if os.path.isdir(src):
			raise IsDirectoryError("copyfile called to copy a directory (%s)" % (src))
		
		if not os.path.lexists(src):
			raise InvalidPathError("copyfile called to copy a non existent file '%s'" % (src))
		
		logger.debug("%s> copyfile('%s' to '%s')" % (os.getcwd(), src, dst))
		shutil.copy2(src, dst)
	
	
	@staticmethod
	@askUserOnError
	def rmfile(path):
		p = Filesystem.unixPathToOS(path)
		
		if not os.path.lexists(p):
			return
		
		if os.path.isdir(p):
			raise IsDirectoryError("rmfile called to remove directory '%s'" % (p))
		
		logger.debug("%s> rmfile(%s)" % (os.getcwd(), p))
		os.remove(p)
	
	
	@staticmethod
	@askUserOnError
	def rm(path):
		p = Filesystem.unixPathToOS(path)
		if os.path.isdir(p):
			Filesystem.rmtree(p)
		else:
			Filesystem.rmfile(p)
	
	@staticmethod
	@askUserOnError
	def rename(srcpath, dstpath):
		src = Filesystem.unixPathToOS(srcpath)
		dst = Filesystem.unixPathToOS(dstpath)
		
		if not os.path.lexists(src):
			raise InvalidPathError("rename called to rename a non existent file or directory '%s'" % (src))
		
		if os.path.lexists(dst):
			raise InvalidPathError("rename called to rename to an already existent file or directory '%s'" % (dst))
		logger.debug("%s> rename('%s', '%s')" % (os.getcwd(), src, dst))
		os.rename(src, dst)
