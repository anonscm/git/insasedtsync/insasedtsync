package fr.insa_strasbourg.insasedtsync.ui

import androidx.appcompat.app.AppCompatActivity

interface StartupFragment {

    fun initialize(activity: AppCompatActivity)

}