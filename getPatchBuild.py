#!/usr/bin/python3
# -*- coding: utf-8 -*-

from INSAPatcher import *
from pkg_resources import parse_version


logger.setLevel(logging.INFO)



#################
# Main settings #
#################

class Settings:
	""" Settings of script """
	
	BASEDIR=os.getcwd() + os.sep
	""" App startup directory"""
	
	VERSION="latest"
	""" Version (git tag) to retrieve, or "latest" to retrieve latest
	tag """
	
	GITSRCURL="https://gitlab.com/bitfireAT/icsx5.git"
	""" URL of upstream git repo """
	
	UPSTREAMSRCDIR="icsx5"
	""" Name of upstream's source directory """
	
	WORKDIR="work"
	""" Work directory """
	
	BUILDDIR="build"
	""" APKs build directory """
	
	RELEASEDIR="release"
	""" APKs save directory """
	
	SOURCEDIR="src"
	""" Source directory """
	
	BUILDTEMPLATEDIR="build-template"
	""" Directory containing files/dir to append in build-directory
	(ie. Android Studio config dirs) """
	
	APKBUILDNUMBERCACHEFILE = 'latestbuild.txt'
	""" File used to store latest apk build number """
	
	BUILDNUMBER = Android.getNextBuildNumber(APKBUILDNUMBERCACHEFILE)
	""" APK build number """
	
	SOURCESEXTENSIONS = ('.xml', '.java', '.kt', '.gradle', 'aidl', '.StartupFragment')
	""" Extensions to source files to patch """
	
	APKDEBUGSRC = Filesystem.unixPathToOS(BUILDDIR + '/app/build/outputs/apk/debug/app-debug.apk')
	""" Path to debug apk, once built """
	
	APKUNSIGNEDSRC = Filesystem.unixPathToOS(BUILDDIR + '/app/build/outputs/apk/standard/release/app-standard-release-unsigned.apk')
	""" Path to release apk, once built """
	
	


#################################
# Retrieve latest ICSx⁵ sources #
#################################
def getICSx5Sources():
	logger.warning("Retrieving latest ICSx⁵ sources")
	os.chdir(Settings.BASEDIR)
	
	if not os.path.lexists(Settings.UPSTREAMSRCDIR):
		# First download from git repo
		Command.run(["git", "clone", "--recursive", Settings.GITSRCURL, "--quiet"])
	else:
		# Update from git repo
		os.chdir(Settings.BASEDIR + Settings.UPSTREAMSRCDIR)
		Command.run(["git", "pull", "--recurse-submodules", "--quiet"])
		os.chdir(Settings.BASEDIR)

	# Copy sources in work directory
	Filesystem.rmtree(Settings.WORKDIR)
	Filesystem.copytree(Settings.UPSTREAMSRCDIR, Settings.WORKDIR)
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)

	# Find latest tag
	if Settings.VERSION == "latest":
		(ret, out, err) = Command.run(["git", "tag", "-l"], stdout=Command.TOVAR)
		versions = out.split("\n")
		while '' in versions:
			versions.remove('')
		tmp = [ "insasedtsync" + v + "insasedtsync" for v in versions ]
		versions = [ v[12:-12] for v in sorted(tmp, key=parse_version) ]

		Settings.VERSION=""
		while not Settings.VERSION:
			Settings.VERSION = versions.pop()

	logger.error("*** Using ICSx⁵ %s ***" % (Settings.VERSION))

	# Tag selection
	Command.run(["git", "checkout", "tags/%s" % (Settings.VERSION), "--quiet"])
	Command.run(["git", "submodule", "update", "--quiet"])

	# Remove ICSx⁵ git files
	for d in (".", "cert4android", "ical4android"):
		for sub in ('.git', '.gitignore', '.gitlab-ci.yml', '.gitmodules'):
			Filesystem.rm(d + os.sep + sub)









###################################
# Convert ICSx⁵ into insasedtsync #
###################################
def convertICSx5ToInsasedtsync():
	logger.warning("Converting ICSx⁵ into insasedtsync")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	
	logger.info(" - Updating build versionCode")
	s = SourceFile('app/build.gradle')
	s.replaceByRegex(
		pattern = r'^( +versionCode) .*$',
		replacement = r'\1 %s' % (Settings.BUILDNUMBER)
	)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Renaming app")
	for f in Filesystem.findFiles(".", suffixes=Settings.SOURCESEXTENSIONS, filesOnly=True):
		s = SourceFile(f)
		s.enqueueReplace(old = 'bitfire.at', new = 'insa-strasbourg.fr', failsWhenUnchanged=False)
		s.enqueueReplace(old = 'at.bitfire.icsdroid', new = 'fr.insa_strasbourg.insasedtsync', failsWhenUnchanged = False)
		s.enqueueReplace(old = 'ICSx⁵', new = 'insasEdtSync', failsWhenUnchanged = False)
		s.enqueueReplace(old = 'ICSx5', new = 'insasEdtSync', failsWhenUnchanged = False)
		s.enqueueReplace(old = 'icsx5.insa-strasbourg.fr', new = 'icsx5.bitfire.at', failsWhenUnchanged = False)
		s.processReplaceQueue()

	logger.info(" - Renaming directories")
	Filesystem.rename('app/src/main/java/at/bitfire/icsdroid', 'app/src/main/java/at/bitfire/insasedtsync')
	Filesystem.rename('app/src/main/java/at/bitfire', 'app/src/main/java/at/insa_strasbourg')
	Filesystem.rename('app/src/main/java/at', 'app/src/main/java/fr')
	
	Filesystem.rename('app/src/standard/java/at/bitfire/icsdroid', 'app/src/standard/java/at/bitfire/insasedtsync')
	Filesystem.rename('app/src/standard/java/at/bitfire', 'app/src/standard/java/at/insa_strasbourg')
	Filesystem.rename('app/src/standard/java/at', 'app/src/standard/java/fr')
	
	Filesystem.rename('app/src/standard/resources/META-INF/services/at.bitfire.icsdroid.ui.StartupFragment', 'app/src/standard/resources/META-INF/services/fr.insa-strasbourg.insasedtsync.ui.StartupFragment')
	
	
	logger.info(" - Renaming app")
	for f in Filesystem.findFiles(".", suffixes=('.xml',), filesOnly=True):
		s = SourceFile(f)
		s.enqueueReplace(old = 'insasEdtSync', new = 'EdT INSA Strasbourg', failsWhenUnchanged = False)
		s.processReplaceQueue()
	
	logger.info(" - Updating icons")
	Filesystem.rm(r"app/src/main/res/mipmap-anydpi-v26")
	Filesystem.copydircontent(r'../icons/res/', r"app/src/main/res/")
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Adding fr.insa_strasbourg.** to proguard's rules")
	s = SourceFile('app/proguard-rules.pro')
	original = """
-keep class at.bitfire.** { *; }	# all ICSx⁵ code is required"""
	patched = """
-keep class at.bitfire.** { *; }	# all ICSx⁵ code is required
-keep class fr.insa_strasbourg.** { *; }       # all insasEdtSync code is required"""
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------
	
	logger.info(" - Removing references to ICSx⁵'s twitter account, website in UI")
	s = SourceFile('app/src/main/res/menu/app_info_activity.xml')
	original = """
<menu xmlns:android="http://schemas.android.com/apk/res/android"
      xmlns:app="http://schemas.android.com/apk/res-auto">

    <item app:showAsAction="ifRoom"
          android:icon="@drawable/ic_public"
          android:onClick="showWebSite"
          android:title="@string/app_info_web_site" />

    <item app:showAsAction="ifRoom"
          android:icon="@drawable/twitter_white"
          android:onClick="showTwitter"
          android:title="@string/app_info_twitter"/>

</menu>"""

	patched = """
<menu xmlns:android="http://schemas.android.com/apk/res/android"
      xmlns:app="http://schemas.android.com/apk/res-auto">

    <!--
    <item app:showAsAction="ifRoom"
          android:icon="@drawable/ic_public"
          android:onClick="showWebSite"
          android:title="@string/app_info_web_site" />

    <item app:showAsAction="ifRoom"
          android:icon="@drawable/twitter_white"
          android:onClick="showTwitter"
          android:title="@string/app_info_twitter"/>
    -->

</menu>"""
	
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(""" - Appending insasedtsync's "About" tab, and complete ICSx⁵'s "About" tab (app name, website, license)""")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/insasedtsync/ui/InfoActivity.kt')
	original = """
                arrayOf("insasEdtSync", "insasEdtSync/${BuildConfig.VERSION_NAME}", "Ricki Hirner, Bernhard Stockmann (insa-strasbourg.fr)", "https://icsx5.bitfire.at", "gpl-3.0-standalone.html"),
"""
	patched = """
                arrayOf("EdT INSA Strasbourg", "EdT INSA Strasbourg/${BuildConfig.VERSION_NAME}", "Code original de ICSx⁵ adapté par Boris Lechner (INSA Strasbourg)", "https://sourcesup.renater.fr/insasedtsync/", "gpl-3.0-standalone.html"),
                arrayOf("ICSx⁵", "ICSx⁵/${BuildConfig.VERSION_NAME}", "Ricki Hirner, Bernhard Stockmann (bitfire.at)", "https://icsx5.bitfire.at", "gpl-3.0-standalone.html"),
"""
	s.replace(original, patched)
	
	
	#-------------------------------------------------------------------
	
		
	# Change message suggesting to donate in english and french languages,
	# delete this message in other languages in order to display english version,
	# because I'm unable to translate it in other languages
	logger.info(" - Updating message suggesting to donate")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR + "/app/src/main/res/")
	for d in Filesystem.findFiles(".", prefixes=("values"), dirsOnly=True):
		f = Filesystem.unixPathToOS("%s/strings.xml" % (d))
		if os.path.isfile(f):
			s = SourceFile(f)
			if d.endswith("values"):
				s.enqueueReplaceByRegex(
					pattern = r'(<string name="donate_message">).*(</string>)',
					replacement = r'\1This software is heavily based on ICSx⁵ app. If you find EdT INSA Strasbourg useful, please consider buying ICSx⁵ app on Google Play.\2'
				)
				s.enqueueReplaceByRegex(
					pattern = r'(<string name="donate_now">).*(</string>)',
					replacement = r'\1Show app on Play Store\2'
				)
			elif d.endswith("values-fr"):
				s.enqueueReplaceByRegex(
					pattern = r'(<string name="donate_message">).*(</string>)',
					replacement = r'''\1Cette application n\'est qu\'une légère adaptation d\'une autre application : ICSx⁵. Si EdT INSA Strasbourg vous rend service, merci d\'envisager d\'acheter leur application sur Google Play.\2'''
				)
				s.enqueueReplaceByRegex(
					pattern = r'(<string name="donate_now">).*(</string>)',
					replacement = r'''\1Ouvrir le Play Store\2'''
				)
			else:
				s.enqueueReplaceByRegex(
					pattern = r'^.*(<string name="donate_message">).*(</string>).*$',
					replacement = r'',
					failsWhenUnchanged = False
				)
				s.enqueueReplaceByRegex(
					pattern = r'^.*(<string name="donate_now">).*(</string>).*$',
					replacement = r'',
					failsWhenUnchanged = False
				)
			
			s.enqueueReplaceByRegex(
				pattern = r'^.*(<string name="app_info_donate">).*(</string>).*$',
				replacement = r'',
				failsWhenUnchanged = False
			)
			s.processReplaceQueue()
			
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	
	#-------------------------------------------------------------------


	logger.info(""" - Adding translations""")
	s = SourceFile('app/src/main/res/values-fr/strings.xml')
	original = """
  <string name="calendar_list_sync_disabled">Synchro. désactivée</string>
"""
	patched = """
  <string name="calendar_list_sync_disabled">Synchro. désactivée</string>
  <string name="add_calendar_ressourceid">Identifiant(s) de ressource</string>
"""
	s.enqueueReplace(original, patched)
	
	original = r"""
  <string name="calendar_list_empty_info">Pour s\'abonner à un flux Webcal, utilisez le bouton + ou ouvrez une URL Webcal</string>
"""
	patched = """
  <string name="calendar_list_empty_info">Pour ajouter un emploi du temps, utilisez le bouton +</string>
"""
	s.enqueueReplace(original, patched)
	
	original = """
  <string name="add_calendar_need_valid_uri">URL valide requise</string>
"""
	patched = r"""
  <string name="add_calendar_need_valid_uri">Format d\'identifiant(s) de ressource invalide</string>
"""
	s.enqueueReplace(original, patched)
	
	s.processReplaceQueue()
	
	
	s = SourceFile('app/src/main/res/values/strings.xml')
	original = """
    <string name="add_calendar_need_valid_uri">Valid URI required</string>
"""
	patched = """
    <string name="add_calendar_need_valid_uri">Valid ressource identifier(s) required</string>
    <string name="add_calendar_ressourceid">Ressource identifier(s)</string>
"""
	s.replace(original, patched)


	#-------------------------------------------------------------------








###############################
# Apply INSA theme to insasedtsync #
###############################
def updateTheme():
	logger.warning("Applying insasedtsync's theme")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	
	logger.info(" - Adding theme colors")
	s = SourceFile('app/src/main/res/values/colors.xml')
	original = """
    <color name="lightblue">#039be5</color>
    <color name="darkblue">#01579b</color>

	<color name="redorange">#ff2200</color>
"""
	patched = """
    <color name="lightblue">#039be5</color>
    <color name="darkblue">#01579b</color>

	<color name="redorange">#ff2200</color>

    <color name="red700_insa">#b71c1c</color>
    <color name="light_red_insa700">#c62828</color>
    <color name="light_red_insa500">#e42618</color>
    <color name="grey_insa500">#9e9e9e</color>
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	#-------------------------------------------------------------------


	logger.info(" - Using theme colors")
	for f in Filesystem.findFiles(".", suffixes=Settings.SOURCESEXTENSIONS, filesOnly=True):
		s = SourceFile(f)
		s.enqueueReplace(old = '@color/lightblue', new = '@color/light_red_insa700', failsWhenUnchanged = False)
		s.enqueueReplace(old = '@color/darkblue', new = '@color/red700_insa', failsWhenUnchanged = False)
		s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------




#########################
# Changing app behavior #
#########################
def updateAppBehavior():
	logger.warning("Changing app behavior")
	os.chdir(Settings.BASEDIR + Settings.WORKDIR)
	
	
	logger.info(" - Disabling authentication settings")
	
	s = SourceFile('app/src/main/res/layout/credentials.xml')
	original = """
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent">
"""
	patched = """
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:visibility="gone">
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	# Add URL layout
	s = SourceFile('app/src/main/res/layout/add_calendar_enter_url.xml')

	original = """
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/add_calendar_url"/>

        <EditText
            android:id="@+id/url"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:hint="https://example.com/calendar.ics"
            android:inputType="textUri"
            android:singleLine="true"
            tools:ignore="HardcodedText"/>

        <TextView
            android:id="@+id/insecure_authentication_warning"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:drawableLeft="@drawable/ic_warning"
            android:drawableStart="@drawable/ic_warning"
            android:drawablePadding="8dp"
            android:padding="16dp"
            android:text="@string/add_calendar_authentication_without_https_warning"
            android:textAppearance="?android:attr/textAppearanceMedium"
            android:visibility="gone"/>

        <FrameLayout
            android:id="@+id/credentials"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />

    </LinearLayout>
"""
	patched = """
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">

        <TextView
            android:id="@+id/textView"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/add_calendar_url" />

        <EditText
            android:id="@+id/baseurl"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:enabled="false"
            android:hint="https://dsi.insa-strasbourg.fr/outils/ade/synchronisation-ics.php?id="
            android:inputType="textUri"
            android:singleLine="true"
            android:text="https://dsi.insa-strasbourg.fr/outils/ade/synchronisation-ics.php?id="
            tools:ignore="HardcodedText" />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/add_calendar_ressourceid" />

        <EditText
            android:id="@+id/url"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:hint="2124,23"
            android:inputType="text"
            android:singleLine="true"
            tools:ignore="HardcodedText" />

        <TextView
            android:id="@+id/lnkDoc"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:autoLink="web"
            android:text="Documentation : https://dsi.insa-strasbourg.fr/outils/ade/" />

        <TextView
            android:id="@+id/insecure_authentication_warning"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:drawableLeft="@drawable/ic_warning"
            android:drawableStart="@drawable/ic_warning"
            android:drawablePadding="8dp"
            android:padding="16dp"
            android:text="@string/add_calendar_authentication_without_https_warning"
            android:textAppearance="?android:attr/textAppearanceMedium"
            android:visibility="gone"/>

        <FrameLayout
            android:id="@+id/credentials"
            android:layout_width="match_parent"
            android:layout_height="wrap_content" />

    </LinearLayout>
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Forcing URL and only ask user for ressource id(s)")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/insasedtsync/ui/AddCalendarEnterUrlFragment.kt')
	original = """
            uri = URI(v.url.text.toString())
            uri.scheme.equals("file", true) || uri.scheme.equals("http", true) || uri.scheme.equals("https", true)
"""
	patched = '''
            uri = URI(v.baseurl.text.toString() + v.url.text.toString())
            v.url.text.toString().matches(Regex("""\d+(,\d+)*"""))
            // uri.scheme.equals("file", true) || uri.scheme.equals("http", true) || uri.scheme.equals("https", true)
'''
	s.enqueueReplace(original, patched)



	original = """
            uri = URI(view.url.text.toString())
"""
	patched = '''
            uri = URI(view.baseurl.text.toString() + view.url.text.toString())
'''
	s.enqueueReplace(original, patched)	



	original = """
            info.url = URL(view!!.url.text.toString())
"""
	patched = '''
            info.url = URL(view!!.baseurl.text.toString() + view!!.url.text.toString())
'''
	s.enqueueReplace(original, patched)


	
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Forcing default calendar title to 'EdT INSA'")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/insasedtsync/ui/AddCalendarDetailsFragment.kt')
	original = """
            title = activity?.intent?.getStringExtra(EXTRA_TITLE) ?: info.calendarName
"""
	patched = '''
            title = activity?.intent?.getStringExtra(EXTRA_TITLE) ?: "EdT INSA"
'''
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Forcing default calendar color to INSA's red")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/insasedtsync/db/LocalCalendar.kt')
	original = """
        const val DEFAULT_COLOR = 0xFF2F80C7.toInt()
"""
	patched = '''
        const val DEFAULT_COLOR = 0xFFE42618.toInt()
'''
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Forcing default sync interval twice a day and disabling time sync menu")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/insasedtsync/AppAccount.kt')
	original = """
    const val SYNC_INTERVAL_MANUALLY = -1L
"""
	patched = """
    const val SYNC_INTERVAL_MANUALLY = -1L
    const val SYNC_INTERVAL_DAILY = 86400L
"""
	s.enqueueReplace(original, patched)
	
	
	original = """
        var syncInterval = SYNC_INTERVAL_MANUALLY
"""
	patched = """
        var syncInterval = SYNC_INTERVAL_DAILY
"""
	s.enqueueReplace(original, patched)
	
	s.processReplaceQueue()
	
	
	
	s = SourceFile('app/src/main/res/menu/activity_calendar_list.xml')
	original = """
    <item android:title="@string/calendar_list_set_sync_interval"
          android:onClick="onSetSyncInterval"
          app:showAsAction="never"/>
"""
	patched = """
    <!--
    <item android:title="@string/calendar_list_set_sync_interval"
          android:onClick="onSetSyncInterval"
          app:showAsAction="never"/>
    -->
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Disabling donation requests, to respect Google payment policy")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/insasedtsync/Constants.kt')
	original = """
    val donationUri = Uri.parse("https://icsx5.bitfire.at/donate/?pk_campaign=icsx5-app")!!
"""
	patched = """
    val donationUri = Uri.parse("market://details?id=at.bitfire.icsdroid")!!
"""
	s.replace(original, patched)
	
	
	
	# s = SourceFile('app/src/main/java/fr/insa_strasbourg/insasedtsync/ui/CalendarListActivity.kt')
	# original = """
        # if (getPreferences(0).getLong(DonateDialogFragment.PREF_NEXT_REMINDER, 0) < System.currentTimeMillis()) {
            # val installer = packageManager.getInstallerPackageName(BuildConfig.APPLICATION_ID)
            # if (installer == null || installer.startsWith("org.fdroid"))
                # DonateDialogFragment().show(supportFragmentManager, "donate")
        # }
# """
	# patched = """
        # if (getPreferences(0).getLong(DonateDialogFragment.PREF_NEXT_REMINDER, 0) < System.currentTimeMillis()) {
            # /* val installer = packageManager.getInstallerPackageName(BuildConfig.APPLICATION_ID)
            # if (installer == null || installer.startsWith("org.fdroid"))
            # */
            # DonateDialogFragment().show(supportFragmentManager, "donate")
        # }
# """
	# s.replace(original, patched)
	
	
	#-------------------------------------------------------------------
	
	
	logger.info(" - Forcing timezone of calendars to Paris")
	s = SourceFile('app/src/main/java/fr/insa_strasbourg/insasedtsync/ui/AddCalendarDetailsFragment.kt')
	original = """
        calInfo.put(Calendars.CALENDAR_ACCESS_LEVEL, Calendars.CAL_ACCESS_READ)
"""
	patched = """
        calInfo.put(Calendars.CALENDAR_ACCESS_LEVEL, Calendars.CAL_ACCESS_READ)
        calInfo.put(Calendars.CALENDAR_TIME_ZONE, "Europe/Paris")
"""
	s.enqueueReplace(original, patched)
	s.processReplaceQueue()
	
	
	#-------------------------------------------------------------------
	
	
	
	



#####################
# Source dir update #
#####################
def saveInsasedtsyncSources():
	logger.warning("Saving source dir")
	os.chdir(Settings.BASEDIR)
	Filesystem.rmtree(Settings.SOURCEDIR)
	Filesystem.rename(Settings.WORKDIR, Settings.SOURCEDIR)
	
	



##############
# Build APKs #
##############
def buildApks():
	logger.warning("Building apks")
	os.chdir(Settings.BASEDIR)
	
	# Creating build dir
	Filesystem.rmtree(Settings.BUILDDIR)
	Filesystem.copytree(Settings.SOURCEDIR, Settings.BUILDDIR)
	if os.path.isdir(Settings.BUILDTEMPLATEDIR):
		Filesystem.copydircontent(Settings.BUILDTEMPLATEDIR, Settings.BUILDDIR)
		
	# Paths
	apkdebug = Filesystem.unixPathToOS(Settings.RELEASEDIR + '/insasedtsync-%s-debug.apk' % (Settings.VERSION))
	apkunsigned = Filesystem.unixPathToOS(Settings.RELEASEDIR + '/insasedtsync-%s-release-unsigned.apk' % (Settings.VERSION))
	apksigned = Filesystem.unixPathToOS(Settings.RELEASEDIR + '/insasedtsync-%s-release-signed.apk' % (Settings.VERSION))
	
	
	#~ # Build debug apk
	#~ logger.info(" - Building debug APK")
	#~ os.chdir(Settings.BASEDIR + Settings.BUILDDIR)
	#~ Command.run(["gradlew.bat", "assembleDebug"])
	#~ os.chdir(Settings.BASEDIR)
	#~ Filesystem.rmfile(apkdebug)
	#~ Filesystem.rename(Settings.APKDEBUGSRC, apkdebug)
	
	
	# Build unsigned release apk
	logger.info(" - Building release APK")
	os.chdir(Settings.BASEDIR + Settings.BUILDDIR)
	Command.run(["gradlew.bat", "assembleRelease"])
	os.chdir(Settings.BASEDIR)
	Filesystem.rmfile(apkunsigned)
	Filesystem.rename(Settings.APKUNSIGNEDSRC, apkunsigned)

	# Sign release APK
	logger.info(" - Signing release APK")
	Android.signApk(apkunsigned, apksigned, "keystore.jks", "key0")

	# Delete unsigned APK
	logger.info(" - Deleting unsigned APK")
	Filesystem.rmfile(apkunsigned)

	# Save current build number as latest
	Android.commitCurrentBuild(Settings.APKBUILDNUMBERCACHEFILE)




def clean():
	# Stop all running gradle daemons
	if os.path.isdir(Settings.BASEDIR + Settings.BUILDDIR):
		os.chdir(Settings.BASEDIR + Settings.BUILDDIR)
		Command.run(["gradlew.bat", "--stop"])
	elif os.path.isdir(Settings.BASEDIR + Settings.SOURCEDIR):
		os.chdir(Settings.BASEDIR + Settings.SOURCEDIR)
		Command.run(["gradlew.bat", "--stop"])
	
	
	os.chdir(Settings.BASEDIR)
	Filesystem.mkdir(Settings.RELEASEDIR)
	Filesystem.rmtree(Settings.BUILDDIR)
	Filesystem.rmtree(Settings.WORKDIR)
	Filesystem.rmtree("__pycache__")




if __name__ == '__main__':
	try:
		clean()
		getICSx5Sources()
		convertICSx5ToInsasedtsync()
		updateTheme()
		updateAppBehavior()
		saveInsasedtsyncSources()
		buildApks()
	except SystemExit:
		pass
	except KeyboardInterrupt:
		logger.error("Keyboard interrupt")
	finally:
		clean()
